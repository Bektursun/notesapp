package com.bektursun.notesapp.data.model

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "notes")
@Parcelize
data class NoteModel(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    val noteId: Long? = null,
    @ColumnInfo(name = "content")
    val content: String
) : Parcelable