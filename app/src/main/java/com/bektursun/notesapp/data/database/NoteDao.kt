package com.bektursun.notesapp.data.database

import androidx.lifecycle.LiveData
import androidx.room.*
import com.bektursun.notesapp.data.model.NoteModel

@Dao
interface NoteDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertNote(note: NoteModel)

    @Update
    suspend fun updateNote(vararg note: NoteModel)

    @Delete
    suspend fun deleteNote(note: NoteModel)

    @Query("SELECT * FROM notes WHERE id == :id")
    fun getNoteById(id: Long): NoteModel

    @Query("SELECT * FROM notes")
    fun getNotes(): LiveData<List<NoteModel>>

    @Query("DELETE FROM notes WHERE id == :id")
    suspend fun deleteNoteById(id: Long)
}