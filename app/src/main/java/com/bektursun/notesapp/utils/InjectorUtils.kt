package com.bektursun.notesapp.utils

import android.app.Application
import com.bektursun.notesapp.ui.fragment.MainViewModelFactory

object InjectorUtils {

    fun provideViewModelFactory(application: Application) : MainViewModelFactory {
        return MainViewModelFactory(application)
    }
}