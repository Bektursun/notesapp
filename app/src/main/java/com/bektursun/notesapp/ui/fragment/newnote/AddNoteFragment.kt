package com.bektursun.notesapp.ui.fragment.newnote

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.bektursun.notesapp.data.model.NoteModel
import com.bektursun.notesapp.databinding.AddNoteFragmentBinding

class AddNoteFragment : Fragment() {

    private lateinit var viewModel: AddNoteViewModel
    private lateinit var binding: AddNoteFragmentBinding
    private val args: AddNoteFragmentArgs by navArgs()

    private var updateOrInsert: Boolean = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = AddNoteFragmentBinding.inflate(inflater, container, false)
        viewModel = AddNoteViewModel(activity?.application!!)
        validator()
        return binding.root
    }

    override fun onDestroyView() {
        insertNote()
        Log.d("NewNote", "onDestroy")
        super.onDestroyView()
    }

    private fun validator() {
        if (args.note != null) {
            updateOrInsert = true
            binding.contentEditText.setText(args.note!!.content)
        }
    }

    private fun insertNote() {
        if (binding.contentEditText.text.isNotEmpty() && args.note == null) {
            updateOrInsert = false
            val note = NoteModel(
                content = binding.contentEditText.text.toString()
            )
            viewModel.insertNote(note)
            Log.d("NewNote", "Insert new note")
        } else {
            val updatedNote = NoteModel(
                noteId = args.note?.noteId,
                content = binding.contentEditText.text.toString()
            )
            viewModel.updateNote(updatedNote)
            Log.d("NewNote", "Update current note")
        }
    }

}
