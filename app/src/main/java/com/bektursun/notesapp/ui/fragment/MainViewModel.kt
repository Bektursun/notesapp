package com.bektursun.notesapp.ui.fragment

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.bektursun.notesapp.data.database.NoteDatabase
import com.bektursun.notesapp.data.model.NoteModel
import com.bektursun.notesapp.repository.NoteRepository

class MainViewModel(application: Application) : AndroidViewModel(application) {

    private var repository: NoteRepository
    val allNotes: LiveData<List<NoteModel>>

    init {
        val noteDao = NoteDatabase.getNoteDatabase(application.applicationContext).noteDao()
        repository = NoteRepository(noteDao)
        allNotes = repository.allNotes()
    }

}
