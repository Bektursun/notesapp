package com.bektursun.notesapp.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.observe
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.bektursun.notesapp.R
import com.bektursun.notesapp.databinding.MainFragmentBinding

class MainFragment : Fragment() {

    private lateinit var mainViewModel: MainViewModel
    private lateinit var binding: MainFragmentBinding
    private lateinit var noteAdapter: NoteAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.main_fragment, container, false)
        noteAdapter = NoteAdapter()
        mainViewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        setupRV(noteAdapter)

        binding.addNoteFab.setOnClickListener {
            val direction =  MainFragmentDirections.actionMainFragmentToAddNoteFragment(null)
            findNavController().navigate(direction)
        }

        return binding.root
    }

    private fun setupRV(adapter: NoteAdapter) {
        binding.noteListRV.adapter = noteAdapter
        mainViewModel.allNotes.observe(viewLifecycleOwner) { routes ->
            adapter.submitList(routes)
        }
    }
}
