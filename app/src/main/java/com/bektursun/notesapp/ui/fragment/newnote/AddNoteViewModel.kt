package com.bektursun.notesapp.ui.fragment.newnote

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bektursun.notesapp.data.database.NoteDatabase
import com.bektursun.notesapp.data.model.NoteModel
import com.bektursun.notesapp.repository.NoteRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AddNoteViewModel(application: Application) : ViewModel() {

    private val noteRepository: NoteRepository

    init {
        val noteDao = NoteDatabase.getNoteDatabase(application.applicationContext).noteDao()
        noteRepository = NoteRepository(noteDao)
    }

    fun insertNote(note: NoteModel) = viewModelScope.launch(Dispatchers.IO) {
        noteRepository.insertNote(note)
    }

    fun updateNote(note: NoteModel) = viewModelScope.launch(Dispatchers.IO) {
        noteRepository.updateNote(note)
    }
}
