package com.bektursun.notesapp.ui.fragment

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bektursun.notesapp.data.model.NoteModel
import com.bektursun.notesapp.databinding.NoteItemBinding

class NoteAdapter : ListAdapter<NoteModel, NoteAdapter.NoteViewHolder>(NotesDiffCallback()) {

    inner class NoteViewHolder(private val binding: NoteItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        init {
            binding.setClickListener {
                binding.note?.let { note ->
                    navigateToDetailNote(note, it)
                }
            }
        }

        private fun navigateToDetailNote(note: NoteModel, view: View) {
            Toast.makeText(view.context, note.content, Toast.LENGTH_SHORT).show()
            val direction = MainFragmentDirections.actionMainFragmentToAddNoteFragment(note)
            view.findNavController().navigate(direction)
        }

        fun bind(notes: NoteModel) {
            binding.apply {
                note = notes
                executePendingBindings()
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteViewHolder {
        return NoteViewHolder(
            NoteItemBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: NoteViewHolder, position: Int) {
        val notes = getItem(position)
        holder.bind(notes)
    }
}

private class NotesDiffCallback : DiffUtil.ItemCallback<NoteModel>() {
    override fun areItemsTheSame(oldItem: NoteModel, newItem: NoteModel): Boolean {
        return oldItem.noteId == newItem.noteId
    }

    override fun areContentsTheSame(oldItem: NoteModel, newItem: NoteModel): Boolean {
        return oldItem == newItem
    }

}