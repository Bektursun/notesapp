package com.bektursun.notesapp.repository

import com.bektursun.notesapp.data.database.NoteDao
import com.bektursun.notesapp.data.model.NoteModel

class NoteRepository(private val noteDao: NoteDao){

    fun allNotes() = noteDao.getNotes()

    suspend fun deleteNoteById(noteId: Long) = noteDao.deleteNoteById(noteId)

    suspend fun insertNote(note: NoteModel) = noteDao.insertNote(note)

    suspend fun updateNote(note: NoteModel) = noteDao.updateNote(note)

}